<?php


class Post extends Model
{
    protected $table = "posts";

    public function getAllPost()
    {
        $sql = "SELECT posts.*, 
                COUNT(comments.id) AS `comments_count` 
                FROM `posts` 
                LEFT JOIN `comments` 
                ON comments.post_id=posts.id 
                GROUP BY posts.id 
                ORDER BY date DESC";

        return $this->database->query( $sql );

    }

    public function getPostById($id)
    {
        $sql = "SELECT posts.*,
                COUNT(comments.id) 
                AS `comments_count` 
                FROM `posts` 
                LEFT JOIN `comments` 
                ON comments.post_id=posts.id 
                WHERE posts.id = " . $id;
        $sql .= ' LIMIT 1';

        return $this->database->query( $sql );
    }

    public function getPopularPost()
    {
        $sql = "SELECT posts.*, 
                COUNT(comments.id) AS `comments_count` 
                FROM `posts` 
                LEFT JOIN `comments` 
                ON comments.post_id=posts.id 
                GROUP BY posts.id 
                ORDER BY comments_count DESC
                LIMIT 5";

        return $this->database->query( $sql );

    }

}