<?php

class CommentsController extends Controller
{
    public function __construct( array $data = array())
    {
        parent::__construct($data);
        $this->model = new Comment();
    }

    public function add()
    {
        if(!empty($_POST['text'] && $_POST['author']))
        {
            $this->model->insert($_POST);
            Router::redirect('/posts/view/'. $_POST['post_id']);
        }
    }

}