<?php

class PostsController extends Controller
{
   public function __construct( array $data = array())
   {
       parent::__construct($data);
       $this->model = new Post();
   }

    public function index()
    {
        $posts = new Post();
        $this->data['posts'] = $posts->getAllPost();
        $this->data['popular_posts'] = $posts->getPopularPost();

    }

    public function view()
    {
        $params = App::getRouter()->getParams();

        if (isset($params[0]))
        {
            $id = $params[0];

            $post = new Post();
            $this->data['post'] = $post->getPostById($id);
            $this->data['post'] = reset($this->data['post']);

            $comments = new Comment();
            $this->data['comments'] = $comments->getCommentsPost($id);

        }
    }

    public function add()
    {
        if(!empty($_POST['title'] && $_POST['text'] && $_POST['author']))
        {
            $this->model->insert($_POST);

            Router::redirect('/');

        }

    }

}