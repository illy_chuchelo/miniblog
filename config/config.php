<?php

Config::set('site_name', 'V-jet MiniBlog');

Config::set('routes', array(
    'default' => '',
));

Config::set('default_route', 'default');
Config::set('default_controller', 'posts');
Config::set('default_action', 'index');

Config::set('db.host', ' ');
Config::set('db.user', ' ');
Config::set('db.password', ' ');
Config::set('db.name', ' ');