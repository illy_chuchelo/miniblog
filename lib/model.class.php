<?php

class Model
{
    protected $database;

    protected $table;

    protected $query;

    public function __construct()
    {
        $this->database = App::$db;
    }


    public function getTable()
    {
        return $this->table;
    }


    public function insert( $data = array() )
    {
        $sql = "INSERT INTO " . $this->getTable();
        //INSERT INTO table_name (column1, column2, column3, ...)
        //VALUES (value1, value2, value3, ...);
        $keys     = " (";
        $values   = "VALUES (";
        $last_key = end( array_keys( $data ) );
        foreach ( $data as $key => $value )
        {
            $keys   .= $key;
            $values .= "'".$value."'";
            if ( $key != $last_key )
            {
                $keys   .= ', ';
                $values .= ', ';
            }
        }
        $keys   .= ') ';
        $values .= ')';
        $sql .= $keys . $values;
        //var_dump($sql);die();
        return $this->database->query( $sql );
    }

}